import { model } from "mongoose";
import { schema } from './schemas/user.schema';
import { IUserModel } from '../interface/user.interface'

export default model<IUserModel>('user', schema, 'users', true);