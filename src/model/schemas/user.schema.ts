import { Schema } from "mongoose";
import { IUserModel } from '../../interface/user.interface';

export const schema = new Schema({
    name: {
         type: String,
         required: true
    },
    lastname: {
         type: String,
         required: true
    },
    dni: {
         type: String,
         required: true
    },
    createdAt: {
         type: Date,
         required: false
    },
    modifiedAt: {
         type: Date,
         required: false
    }
  }).pre<IUserModel>('save', function (next) {
    if (this.isNew) {
      this.createdAt = new Date();
    } else {
      this.modifiedAt = new Date();
    }
    next();
  });
  