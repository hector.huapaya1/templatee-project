import { Document } from "mongoose";

export interface IUserModel extends Document {
    name: string;
    lastname: string;
    dni: string;
    createdAt: Date;
    modifiedAt: Date;
  }