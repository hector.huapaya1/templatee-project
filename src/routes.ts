import publicRouter from './routes/public.route';

const applyRoutes = server => {
    server.use('/public', publicRouter);
  };

export default applyRoutes;