import config from './config';
import mongoose from 'mongoose';
mongoose.set('useUnifiedTopology', true);

export const connectDb = () => {
    if (config.DB_URL) {
      return mongoose.connect(
        `${config.DB_URL}`,
        { useNewUrlParser: true, useCreateIndex: true, useFindAndModify: false }
      );
    }
  };
  