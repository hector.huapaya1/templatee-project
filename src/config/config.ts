import 'dotenv/config';

export default {
  PORT: process.env.PORT || 4000,
  HOST: process.env.HOST || 'localhost',
  DB_URL: process.env.DB_URL || 'mongodb+srv://daniel:Mc5BATd74d2tPEJ@cluster0-oyz9l.mongodb.net/test?retryWrites=true&w=majority',
  REDIS_URL: process.env.REDIS_URL || 'redis://localhost:6379/15'
};
