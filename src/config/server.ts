import express from "express";
import bodyParser from 'body-parser';
import applyRoutes from '../routes';

const server = express();
server.use(bodyParser.json());
applyRoutes(server);

export default server;