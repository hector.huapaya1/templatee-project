
import config from './config/config';
import server from '../src/config/server';
import {connectDb} from '../src/config/db.conection'

server.listen(config.PORT, () => {
    connectDb().then(async () => console.log(`Base de datos conectada`));
    console.log(`app running on port ${config.PORT}`);
});