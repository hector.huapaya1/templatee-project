import UserModel from "../model/user.model";
import { IUserModel } from "../interface/user.interface";
import Redis from "../config/redis.config";

class TemplateController {
    async getUsers(req: any, res: any): Promise<any> {
        const users = await UserModel.find({});
        Redis.get("user", (err, result)=>{
            if (err) {
                console.error(err);
            } else {
                console.log(result);
            }
        });
        res.json({
            data:users,
            message: 'success'
        });
    }

    async createUser(req: any, res: any): Promise<any> {
        const user: IUserModel = req.body;
        const newUser = await UserModel.create(user);
        Redis.set("user", newUser.toString());
        res.json({
            data:newUser,
            message: 'success'
        });
    }
};

export default new TemplateController();