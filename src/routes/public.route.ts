import { Router } from 'express';
import UserController from '../controllers/user.controller';

const router = Router();

router.post('/obtener-usuarios', UserController.getUsers);
router.post('/crear-usuario', UserController.createUser);

export default router;
